.. index:: Compositor Nodes; Viewer
.. _bpy.types.CompositorNodeViewer:

***********
Viewer Node
***********

.. figure:: /images/compositing_node-types_CompositorNodeViewer.webp
   :align: right
   :alt: Viewer Node.

The *Viewer* node allows temporarily visualizing data from inside a node graph.
It can be plugged in anywhere to inspect an image or value map in your node tree.

Select a view node with :kbd:`LMB` to switch between multiple viewer nodes.
It is possible to automatically plug any other node into a Viewer node
by pressing :kbd:`Shift-Ctrl-LMB` on it.


Inputs
======

Image
   RGB image. The default is black, so leaving this node unconnected will result in a black image.
Alpha
   Alpha channel.


Properties
==========

Use Alpha
   Used alpha channel, colors are treated alpha *premultiplied*.
   If disabled, alpha channel gets set to 1,
   and colors are treated as alpha *straight*, i.e. color channels does not change.

Tile Order
   The background is computed in small chunks, also referred to as tiles.
   This property determines the starting point and order to render these chunks.

   By prioritizing a certain area, you can make adjustments to the composite
   and see the results faster without having to wait for the whole image to render.

   This property is only accessible in the *Properties* panel in Sidebar region.

   :Rule of thirds:
      Calculates tiles around each of the nine zones defined by the *rule of thirds*.
   :Bottom up:
      Tiles are calculated from the bottom up.
   :Random:
      Calculates tiles in a non-specific order.
   :Center:
      Calculates the tiles starting at a specific focal point, defined by the *X* and *Y* fields.
      Note, the tile center is only visible when the viewer node is selected.

      .. _bpy.types.CompositorNodeViewer.center:

      X, Y
         The position of the center or "focal point" relative to the image bounds; the origin is bottom left.


Outputs
=======

This node has no output sockets.

.. note::

   It is possible to add multiple Viewer nodes, though only the active one
   (last selected, indicated by a red header) will be shown on the backdrop or in the Image editor.


Using the Image Editor
======================

The Viewer node allows results to be displayed in the Image Editor.
The image is facilitated in the header by selecting *Viewer Node* in the linked *Image* data-block menu.
The Image Editor will display the image from the currently selected Viewer node.

To save the image being viewed,
use :menuselection:`Image --> Save As...`, :kbd:`Alt-S` to save the image to a file.

The Image Editor also has three additional options in its header to view Images with or
without Alpha, or to view the Alpha or Z itself.
Click and holding the mouse in the Image displayed allows you to sample the values.
